<?php

namespace App\Transformers;

use App\Models\InvoiceItem;
use App\Models\Item;
use League\Fractal\TransformerAbstract;

class InvoiceItemTransformer extends TransformerAbstract
{
    public $type = 'invoice_item';
    
    protected $availableIncludes = ["items"];

    public function transform(InvoiceItem $model)
    {
        return [
            "id"                    => $model->uuid,
            "quantity"              => $model->quantity,
            "total"                 => $model->total,
        ];
    }

    public function includeItems(InvoiceItem $model) 
    {
        $items = Item::where("id", $model->item_id)->get();
        if (!empty($items)) {
            return $this->collection($items, new ItemTransformer(), "item");
        }
    }
}