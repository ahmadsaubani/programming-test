<?php

namespace App\Transformers;

use App\Models\Item;
use App\Models\Type;
use League\Fractal\TransformerAbstract;

class ItemTransformer extends TransformerAbstract
{
    public $type = 'item';
    
    protected $availableIncludes = ["item_type"];

    public function transform(Item $model)
    {
        return [
            "id"                      => $model->uuid,
            "description"             => $model->description,
            "price"                   => $model->price,
        ];
    }

    public function includeItemType(Item $model)
    {
        $type = Type::find($model->type_id);

        if (!empty($type)) {
            return $this->item($type, new TypeTransformer(), "type");
        } 
    }
}