<?php

namespace App\Transformers;

use App\Models\Item;
use App\Models\Type;
use App\Transformers\ItemTransformer;
use League\Fractal\TransformerAbstract;

class TypeTransformer extends TransformerAbstract
{
    public $type = 'type';
    
    protected $availableIncludes = ["items"];

    public function transform(Type $model)
    {
        return [
            "id"    => $model->uuid,
            "name"  => $model->name,
        ];
    }

    public function includeItems(Type $model) 
    {
        $items = Item::where("type_id", $model->id)->get();
        if (!empty($items)) {
            return $this->collection($items, new ItemTransformer(), "item");
        }
    }
}