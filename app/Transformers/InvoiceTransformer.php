<?php

namespace App\Transformers;

use App\Models\BillingAddress;
use App\Models\Invoice;
use App\Models\InvoiceItem;
use League\Fractal\TransformerAbstract;

class InvoiceTransformer extends TransformerAbstract
{
    public $type = 'invoice';
    
    protected $availableIncludes = ["from_billing_address", "to_billing_address","invoice_items"];

    public function transform(Invoice $model)
    {
        return [
            "id"                        => $model->uuid,
            "no"                        => $model->no,
            "subject"                   => $model->subject,
            "issue_date"                => \Carbon\Carbon::parse($model->issue_date)->format("d/m/Y"),
            "due_date"                  => \Carbon\Carbon::parse($model->due_date)->format("d/m/Y"),
            "sub_total"                 => $model->sub_total,
            "tax"                       => $model->tax . " %",
            "total_with_tax"            => $this->calculateTotalWithTax($model),
            "payments"                  => $model->payments,
            "amount_due"                => $model->amount_due,
            "status"                    => $model->payments == $this->calculateTotalWithTax($model) ? "Paid" : "Unpaid",
        ];
    }

    public function includeFromBillingAddress(Invoice $model)
    {
        $from = BillingAddress::find($model->from_billing_address_id);

        if ($from) {
            return $this->item($from, new BillingAddressTransformer(), 'from_billing_address');
        }
    }

    public function includeToBillingAddress(Invoice $model)
    {
        $to = BillingAddress::find($model->to_billing_address_id);

        if ($to) {
            return $this->item($to, new BillingAddressTransformer(), 'to_billing_address');
        }
    }

    public function includeInvoiceItems(Invoice $model)
    {
        $invoiceItems = InvoiceItem::where("invoice_id", $model->id)->get();
        if ($invoiceItems) {
            return $this->collection($invoiceItems, new InvoiceItemTransformer(), "invoice_item");
        }
    }

    public function calculateTotalWithTax($model)
    {
        $tax         = (int)$model->sub_total * $model->tax / 100;

        return (int)$model->sub_total + $tax . ".00";
    }
    
}