<?php

namespace App\Transformers;

use App\Models\BillingAddress;
use App\Models\Country;
use League\Fractal\TransformerAbstract;

class BillingAddressTransformer extends TransformerAbstract
{
    public $type = 'billing_address';
    
    protected $availableIncludes = ["country"];

    public function transform(BillingAddress $model)
    {
        return [
            "id"                => $model->uuid,
            "name"              => $model->name,
            "street"            => $model->street,
        ];
    }

    public function includeCountry(BillingAddress $model)
    {
       
        $country = Country::find($model->country_id);
        if ($country) {
            return $this->item($country, new CountryTransformer(), "country");
        }
    }
}