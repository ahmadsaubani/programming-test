<?php

namespace App\Transformers;

use App\Models\Country;
use League\Fractal\TransformerAbstract;

class CountryTransformer extends TransformerAbstract
{
    public $type = 'country';
    
    protected $availableIncludes = [];

    public function transform(Country $model)
    {
        return [
            "id"    => $model->uuid,
            "name"  => $model->name
        ];
    }
}