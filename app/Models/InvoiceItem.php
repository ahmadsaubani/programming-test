<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CreateUuid;

class InvoiceItem extends Model
{
    use CreateUuid;
    
    protected $fillable = [
        'invoice_id',
        'item_id',
        "quantity",
        "total"
    ];
}