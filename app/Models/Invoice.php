<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CreateUuid;

class Invoice extends Model
{
    use CreateUuid;
    
    protected $fillable = [
        'no',
        'issue_date',
        'due_date',
        'subject',
        'from_billing_address_id',
        'to_billing_address_id',
        'sub_total',
        'tax',
        'payments',
        'amount_due',
        'status'
    ];
}