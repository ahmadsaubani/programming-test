<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CreateUuid;

class BillingAddress extends Model
{
    use CreateUuid;
    
    protected $fillable = [
        'name',
        'street',
        'country_id',
    ];
}