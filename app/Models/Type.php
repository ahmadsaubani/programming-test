<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CreateUuid;

class Type extends Model
{
    use CreateUuid;
    
    protected $fillable = [
        'name',
    ];
}
