<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BillingAddress;
use App\Transformers\BillingAddressTransformer;

class BillingAddressController extends Controller
{
    public function populate()
    {
        $address = BillingAddress::get();

        $result = $this->collection($address, new BillingAddressTransformer(), "country");

        return $this->showResultV2('Data Found', $result);
    }
}