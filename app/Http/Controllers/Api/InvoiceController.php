<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateInvoiceRequest;
use App\Models\Invoice;
use App\Services\InvoiceService;
use App\Transformers\InvoiceTransformer;
use Exception;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    public function populate()
    {
        $invoices = Invoice::get();

        $result = $this->collection($invoices, new InvoiceTransformer(), "from_billing_address.country,to_billing_address.country,invoice_items.items.item_type");

        return $this->showResultV2('Data Found', $result);
    }

    public function create(CreateInvoiceRequest $request)
    {
        try {
            $request->validated();

            DB::beginTransaction();
            $data =  InvoiceService::createService($request);
            
            $result = $this->item($data, new InvoiceTransformer(), "from_billing_address.country,to_billing_address.country,invoice_items.items.item_type");

            DB::commit();
            return $this->showResultV2('Data Found', $result);
            
        } catch (Exception $e) {
            DB::rollBack();
            return $this->realErrorResponse($e);
        }
        
    }

    public function edit(Request $request, $uuid)
    {
        try {

            DB::beginTransaction();
            $data =  InvoiceService::editInvoiceService($request, $uuid);
            
            $result = $this->item($data, new InvoiceTransformer(), "from_billing_address.country,to_billing_address.country,invoice_items.items.item_type");

            DB::commit();
            return $this->showResultV2('Data Updated', $result);
            
        } catch (Exception $e) {
            DB::rollBack();
            return $this->realErrorResponse($e);
        }
    }

    public function delete($uuid)
    {
        try {
            DB::beginTransaction();
            $data =  Invoice::where("uuid", $uuid)->first();
            
            if (! $data) {
                return $this->errorResponse("Data tidak ditemukan", 404); 
            }

            $data->delete();
            DB::commit();
            return $this->showResult('Success Deleted', []);
            
        } catch (Exception $e) {
            DB::rollBack();
            return $this->realErrorResponse($e);
        }
    }
}