<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateInvoiceRequest extends FormRequest
{
    public function rules()
    {
        return [
            'subject'                   => 'required',
            'issue_date'                => 'required|date_format:Y-m-d',
            'due_date'                  => 'required|date_format:Y-m-d',
            'from_billing_address_id'   => 'required',
            'to_billing_address_id'     => 'required',  
            'items.*'                   => 'required'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            "subject.required"                      => "Subjek harus di isi.",
            "issue_date.required"                   => "Issue date harus di isi.",
            "issue_date.date_format"                => "Issue date harus berformat Y-m-d cth:1996-08-04.",
            "due_date.required"                     => "Due date harus diisi.",
            "due_date.date_format"                  => "Due date harus berformat Y-m-d cth:1996-08-04.",  
            "from_billing_address_id.required"      => "From billing address harus di isi.",
            "to_billing_address_id.required"        => "to billing address harus di isi.",  
            "items.required"                        => "Items harus di isi."
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
