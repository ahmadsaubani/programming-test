<?php

use Database\Seeds\BillingAddressSeeder;
use Database\Seeds\CountriesSeeder;
use Database\Seeds\ItemsSeeder;
use Database\Seeds\TypesSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();
            $this->call(CountriesSeeder::class);
            $this->call(BillingAddressSeeder::class);
            $this->call(TypesSeeder::class);
            $this->call(ItemsSeeder::class);
            
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }
    }
}
