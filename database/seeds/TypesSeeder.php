<?php

namespace Database\Seeds;

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypesSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            "Service"
        ];

        foreach ($categories as $category) {
            Type::create([
                "name" => $category
            ]);
        }
    }
}
