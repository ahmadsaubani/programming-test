<?php

namespace Database\Seeds;

use App\Models\BillingAddress;
use Illuminate\Database\Seeder;

class BillingAddressSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ["Discovery Designs", "41 St Vincent Place Glasglow G1 2ER", 1],
            ["Barrington Publishers", "17 Great Suffolk Street London SE1 0NS", 2],
        ];

        foreach ($data as $value) {
            BillingAddress::create([
                'name'      => $value[0],
                'street'    => $value[1],
                'country_id'   => $value[2],
            ]);
        }
    }
}
