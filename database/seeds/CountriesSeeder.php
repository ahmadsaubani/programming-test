<?php

namespace Database\Seeds;

use App\Models\Country;
use App\Models\Item;
use App\Models\Webinar\Webinar;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CountriesSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            ["Scotland"],
            ["United Kingdom"],
        ];

        foreach ($countries as $country) {
            Country::create([
                "name"     => $country[0]
            ]);
        }
    }
}
