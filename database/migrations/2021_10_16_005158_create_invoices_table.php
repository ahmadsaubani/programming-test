<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->unique()->index();
            $table->string('no')->unique()->index();
            $table->string('subject')->nullable();
            $table->date('issue_date')->nullable();
            $table->date('due_date')->nullable();
            $table->integer("from_billing_address_id")->nullable();
            $table->integer("to_billing_address_id")->nullable();
            $table->decimal('sub_total', 15,2)->unsigned()->default(0);
            $table->integer('tax')->unsigned()->default(0);
            $table->decimal('payments', 15,2)->unsigned()->default(0);
            $table->decimal('amount_due', 15,2)->unsigned()->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}