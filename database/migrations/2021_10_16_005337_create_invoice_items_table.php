<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->unique()->index();
            $table->foreignId('invoice_id')
            ->constrained()
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('item_id')
            ->constrained()
            ->onDelete('cascade')->onUpdate('cascade');
            $table->integer("quantity")->default(1);
            $table->decimal('total', 15,2)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_items');
    }
}
